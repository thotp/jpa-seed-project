package com.thotp.seed.web.service;

import javax.persistence.EntityManager;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.thotp.seed.web.entity.doc.Document;
import com.thotp.seed.web.entity.doc.DocumentFooter;
import com.thotp.seed.web.entity.doc.DocumentHeader;
import com.thotp.seed.web.repository.DocumentRepository;

@RunWith(SpringRunner.class)
@ContextConfiguration(locations = { "classpath:spring/application-config.xml" })
@Rollback(false)
@Transactional
public class ServiceTest {

	@Autowired
	private DocumentRepository documentRepo;

	@Autowired
	private EntityManager em;

	@Test
	public void testSave() {
		Document doc = new Document();
		doc.setDocumentCode(RandomStringUtils.randomAlphanumeric(10));

		DocumentHeader header = new DocumentHeader();
		header.setContent("header");
		header.setDocument(doc);
		doc.setHeader(header);

		DocumentFooter footer = new DocumentFooter();
		footer.setContent("footer");
		footer.setDocument(doc);
		doc.setFooter(footer);

		doc = documentRepo.save(doc);
		
		em.flush();
		em.clear();
		
		doc = documentRepo.findByDocumentCode(doc.getDocumentCode());
		
		Assert.assertNotNull(doc.getHeader());
		Assert.assertNotNull(doc.getFooter());
		
		Assert.assertEquals("header", doc.getHeader().getContent());
		Assert.assertEquals("footer", doc.getFooter().getContent());
	}
}
