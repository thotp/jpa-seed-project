package com.thotp.seed.web.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PostPersist;
import javax.persistence.PostUpdate;
import javax.persistence.Version;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = "employeeId")
@NamedQueries({ @NamedQuery(name = "findByName", query = "select e from Employee e where e.name like :name") })
public class Employee implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Setter(AccessLevel.NONE)
	private Long employeeId;

	private String name;

	@Column(insertable = false, updatable = false)
	@Setter(AccessLevel.NONE)
	private Long departmentId;

	@Version
	@Getter(AccessLevel.NONE)
	@Setter(AccessLevel.NONE)
	private Timestamp _version;

	@ManyToOne
	@JoinColumn(name = "departmentId", referencedColumnName = "departmentId")
	private Department department;

	@OneToMany(mappedBy = "employee", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Address> addresses = new ArrayList<>();
	
	@PostPersist
	@PostUpdate
	private void postPersist() {
		this.departmentId = (department == null) ? null : department.getDepartmentId();
	}
	
	public void setAddresses(List<Address> addresses) {
		if (this.addresses != null) {
			for (Address addr : this.addresses) {
				addr.setEmployee(null);
			}
		}
		if (addresses != null) {
			for (Address addr : addresses) {
				addr.setEmployee(this);
			}
		}
		this.addresses = addresses;
	}
	
	public void addAddress(Address addr) {
		addr.setEmployee(this);
		this.addresses.add(addr);
	}
	
	public void removeAddress(Address addr) {
		this.addresses.remove(addr);
		addr.setEmployee(null);
	}
}
