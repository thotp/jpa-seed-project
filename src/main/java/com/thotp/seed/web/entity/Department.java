package com.thotp.seed.web.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Version;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = { "departmentId" })
public class Department implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Setter(AccessLevel.NONE)
	private Long departmentId;

	private String name;

	@Version
	@Getter(AccessLevel.NONE)
	@Setter(AccessLevel.NONE)
	private Timestamp _version;

	@OneToMany(mappedBy = "department", cascade = CascadeType.ALL, orphanRemoval = false)
	private List<Employee> employees = new ArrayList<>();
	
	public void setEmployees(List<Employee> employees) {
		if (this.employees != null) {
			for (Employee emp : this.employees) {
				emp.setDepartment(null);
			}
		}
		if (employees != null) {
			for (Employee emp : employees) {
				emp.setDepartment(this);
			}
		}
		this.employees = employees;
	}
	
	public void addEmployee(Employee emp) {
		emp.setDepartment(this);
		this.employees.add(emp);
	}
	
	public void removeEmployee(Employee emp) {
		this.employees.remove(emp);
		emp.setDepartment(null);
	}
}
