package com.thotp.seed.web.entity.doc;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PostPersist;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Setter
@Getter
@NoArgsConstructor
@EqualsAndHashCode(of = "documentCode")
public class DocumentFooter implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Setter(AccessLevel.NONE)
	private Long documentId;

	@Column(unique = true, insertable = false, updatable = false)
	private String documentCode;

	private String content;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "documentCode", referencedColumnName = "documentCode")
	private Document document;

	@PostPersist
	private void postPersist() {
		this.documentCode = (document == null) ? null : document.getDocumentCode();
	}
}
