package com.thotp.seed.web.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PostPersist;
import javax.persistence.PostUpdate;
import javax.persistence.Version;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = { "addressId" })
public class Address implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Setter(AccessLevel.NONE)
	private Long addressId;

	@Column(insertable = false, updatable = false, nullable = false)
	@Setter(AccessLevel.NONE)
	private Long employeeId;

	private String line1;

	private String line2;

	private String postalCode;

	@Version
	@Getter(AccessLevel.NONE)
	@Setter(AccessLevel.NONE)
	private Timestamp _version;

	@ManyToOne
	@JoinColumn(name = "employeeId", referencedColumnName = "employeeId", nullable = false)
	private Employee employee;
	
	@PostPersist
	@PostUpdate
	private void postPersist() {
		this.employeeId = (employee == null) ? null : employee.getEmployeeId();
	}
}
