package com.thotp.seed.web.controller;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.thotp.seed.web.entity.Address;
import com.thotp.seed.web.entity.Department;
import com.thotp.seed.web.entity.Employee;
import com.thotp.seed.web.entity.doc.Document;
import com.thotp.seed.web.entity.doc.DocumentFooter;
import com.thotp.seed.web.entity.doc.DocumentHeader;
import com.thotp.seed.web.repository.DocumentRepository;
import com.thotp.seed.web.service.DepartmentService;
import com.thotp.seed.web.service.EmployeeService;

@RestController
@RequestMapping(value = "/hello")
@Transactional(readOnly = true)
public class HelloController {

	@Autowired
	private DepartmentService departmentService;
	@Autowired
	private EmployeeService employeeService;
	@Autowired
	private DocumentRepository documentRepo;

	@PersistenceContext
	private EntityManager em;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ResponseEntity<String> index() {
		return new ResponseEntity<String>("It works!", HttpStatus.OK);
	}

	@RequestMapping(value = "/doSomething", method = RequestMethod.GET)
	@Transactional
	public ResponseEntity<String> doSomething() {
		Department dept = new Department();
		dept.setName("SCM");
		departmentService.save(dept);

		for (int i = 0; i < 1; i++) {
			Employee emp = new Employee();
			emp.setName("Employee " + i);

			Address addr = new Address();
			addr.setLine1("Line 1");
			emp.addAddress(addr);

			dept.addEmployee(emp);
			// employeeService.save(emp);
		}

		departmentService.save(dept);

		// em.refresh(dept);

		for (Department d : departmentService.findAll()) {
			for (Employee emp : d.getEmployees()) {
				System.out.println(emp.getDepartmentId());
				for (Address addr : emp.getAddresses()) {
					System.out.println(addr.getEmployeeId());
				}
			}
		}

		return new ResponseEntity<>(dept.getDepartmentId().toString(), HttpStatus.OK);
	}

	@RequestMapping(value = "/doc", method = RequestMethod.GET)
	@Transactional
	public ResponseEntity<String> doc() {

		Document doc = new Document();
		doc.setDocumentCode(RandomStringUtils.randomAlphanumeric(10));
		
		DocumentHeader header = new DocumentHeader();
		header.setContent("header");
		header.setDocument(doc);
		doc.setHeader(header);
		
		DocumentFooter footer = new DocumentFooter();
		footer.setContent("footer");
		footer.setDocument(doc);
		doc.setFooter(footer);
		
		doc = documentRepo.save(doc);
		
		return new ResponseEntity<>(doc.getDocumentCode(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/doc/{documentCode}", method = RequestMethod.GET)
	@Transactional
	public ResponseEntity<String> read(@PathVariable String documentCode) {

		Document doc = documentRepo.findByDocumentCode(documentCode);
		
		DocumentHeader header = doc.getHeader();
		
		DocumentFooter footer = doc.getFooter();
		
		return new ResponseEntity<>(header.getContent() + " " + footer.getContent(), HttpStatus.OK);
	}

	@RequestMapping(value = "/selectSomething", method = RequestMethod.GET)
	public void selectSomething() {
		for (Department dept : departmentService.findAll()) {
			System.out.println(dept.getDepartmentId() + ": " + dept.getName());
			for (Employee emp : dept.getEmployees()) {
				System.out.println("\t" + emp.getEmployeeId() + ": " + emp.getName());
				for (Address addr : emp.getAddresses()) {
					System.out.println("\t" + addr.getLine1());
				}
			}
		}

	}
}
