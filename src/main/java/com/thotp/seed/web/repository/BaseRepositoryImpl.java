package com.thotp.seed.web.repository;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional(readOnly = true)
public class BaseRepositoryImpl<T, ID> extends SimpleJpaRepository<T, ID> implements BaseRepository<T, ID> {

	private final EntityManager em;

	public BaseRepositoryImpl(JpaEntityInformation<T, ?> entityInformation, EntityManager entityManager) {
		super(entityInformation, entityManager);
		this.em = entityManager;
	}

	@Transactional
	@Override
	public void persist(T entity) {
		em.persist(entity);
	}

	@Transactional
	@Override
	public T merge(T entity) {
		return em.merge(entity);
	}

	@Override
	public List<T> findAll() {
//		System.out.println("BaseRepositoryImpl: " + em.toString());
		return super.findAll();
	}

	@Override
	public List<T> findAll(LockModeType lockMode) {
		return getQuery(null, getDomainClass(), Sort.unsorted()).setLockMode(lockMode).getResultList();
	}

	@Override
	public List<T> findAll(Sort sort, LockModeType lockMode) {
		return getQuery(null, getDomainClass(), sort).setLockMode(lockMode).getResultList();
	}

	@Override
	public <S extends T> S save(S entity) {
//		System.out.println("BaseRepositoryImpl: " + em.toString());
		return super.save(entity);
	}

	@Override
	public Optional<T> findById(ID id, LockModeType lockMode) {
		return Optional.ofNullable(em.find(getDomainClass(), id, lockMode));
	}

}
