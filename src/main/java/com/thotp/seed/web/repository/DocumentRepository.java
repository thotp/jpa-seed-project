package com.thotp.seed.web.repository;

import com.thotp.seed.web.entity.doc.Document;

public interface DocumentRepository extends BaseRepository<Document, Long> {

	Document findByDocumentCode(String documentCode);

}
