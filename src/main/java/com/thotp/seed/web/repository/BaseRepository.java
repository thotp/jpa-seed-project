package com.thotp.seed.web.repository;

import java.util.List;
import java.util.Optional;

import javax.persistence.LockModeType;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface BaseRepository<T, ID> extends JpaRepository<T, ID> {

	/**
	 * JPA's {@code persist}, use with care!
	 * 
	 * @param entity
	 */
	void persist(T entity);

	/**
	 * JPA's {@code merge}, use with care! {@link }
	 * 
	 * @param entity
	 * @return
	 */
	T merge(T entity);

	List<T> findAll(LockModeType lockMode);

	Optional<T> findById(ID id, LockModeType lockMode);

	List<T> findAll(Sort sort, LockModeType lockMode);
}
