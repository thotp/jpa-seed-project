package com.thotp.seed.web.repository.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.thotp.seed.web.entity.Employee;

@Repository
@Transactional(readOnly = true)
public class EmployeeDaoImpl implements EmployeeDao {

	@PersistenceContext
	private EntityManager em;

	@Override
	public List<Employee> findByName(String name) {
		return em.createNamedQuery("findByName", Employee.class).setParameter("name", "%" + name + "%").getResultList();
	}

}
