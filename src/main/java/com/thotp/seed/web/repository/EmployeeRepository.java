package com.thotp.seed.web.repository;

import java.util.List;

import com.thotp.seed.web.entity.Employee;
import com.thotp.seed.web.repository.dao.EmployeeDao;

public interface EmployeeRepository extends BaseRepository<Employee, Long>, EmployeeDao {

	List<Employee> findByName(String name);
}
