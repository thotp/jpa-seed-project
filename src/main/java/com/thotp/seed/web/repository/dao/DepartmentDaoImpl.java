package com.thotp.seed.web.repository.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true)
public class DepartmentDaoImpl implements DepartmentDao {

	@PersistenceContext
	private EntityManager em;

	@Override
	public void customMethod() {
//		System.out.println("DepartmentDaoImpl: " + em.toString());
	}

}
