package com.thotp.seed.web.repository;

import com.thotp.seed.web.entity.Department;
import com.thotp.seed.web.repository.dao.DepartmentDao;

public interface DepartmentRepository extends BaseRepository<Department, Long>, DepartmentDao {

}
