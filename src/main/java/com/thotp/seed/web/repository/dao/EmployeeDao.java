package com.thotp.seed.web.repository.dao;

import java.util.List;

import com.thotp.seed.web.entity.Employee;

public interface EmployeeDao {

	List<Employee> findByName(String name);
}
