package com.thotp.seed.web.service;

import java.util.List;

import com.thotp.seed.web.entity.Employee;

public interface EmployeeService {

	List<Employee> findAll();

	List<Employee> findByName(String name);

	Employee getById(Long employeeId);

	Employee save(Employee employee);

	void remove(Employee employee);

	void remove(Long employeeId);
}
