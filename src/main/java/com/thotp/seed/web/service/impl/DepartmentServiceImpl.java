package com.thotp.seed.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thotp.seed.web.entity.Department;
import com.thotp.seed.web.repository.DepartmentRepository;
import com.thotp.seed.web.service.DepartmentService;

@Service
@Transactional(readOnly = true)
public class DepartmentServiceImpl implements DepartmentService {

	@Autowired
	private DepartmentRepository repo;

	@Override
	public List<Department> findAll() {
		repo.customMethod();
		return repo.findAll();
	}

	@Transactional
	@Override
	public Department save(Department dept) {
		repo.customMethod();
		return repo.save(dept);
	}

}
