package com.thotp.seed.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thotp.seed.web.entity.Employee;
import com.thotp.seed.web.repository.EmployeeRepository;
import com.thotp.seed.web.service.EmployeeService;

@Service
@Transactional(readOnly = true)
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeRepository repo;

	@Override
	public List<Employee> findAll() {
		return repo.findAll();
	}

	@Override
	public List<Employee> findByName(String name) {
		return repo.findByName(name);
	}

	@Override
	public Employee getById(Long employeeId) {
		return repo.findById(employeeId).get();
	}

	@Transactional
	@Override
	public Employee save(Employee employee) {
		return repo.save(employee);
	}

	@Transactional
	@Override
	public void remove(Employee employee) {
		repo.delete(employee);
	}

	@Transactional
	@Override
	public void remove(Long employeeId) {
		repo.deleteById(employeeId);
	}
}
