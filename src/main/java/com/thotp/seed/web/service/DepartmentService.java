package com.thotp.seed.web.service;

import java.util.List;

import com.thotp.seed.web.entity.Department;

public interface DepartmentService {

	List<Department> findAll();

	Department save(Department dept);
}
