package com.thotp.seed.web.config;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.boot.model.naming.PhysicalNamingStrategy;
import org.hibernate.engine.jdbc.env.spi.JdbcEnvironment;

public class HibernatePhysicalNamingStrategyImpl implements PhysicalNamingStrategy {

	@Override
	public Identifier toPhysicalCatalogName(Identifier name, JdbcEnvironment jdbcEnvironment) {
		return (name == null) ? null : Identifier.toIdentifier(format(name.getText()));
	}

	@Override
	public Identifier toPhysicalSchemaName(Identifier name, JdbcEnvironment jdbcEnvironment) {
		return (name == null) ? null : Identifier.toIdentifier(format(name.getText()));
	}

	@Override
	public Identifier toPhysicalTableName(Identifier name, JdbcEnvironment jdbcEnvironment) {
		return (name == null) ? null : Identifier.toIdentifier(format(name.getText()));
	}

	@Override
	public Identifier toPhysicalSequenceName(Identifier name, JdbcEnvironment jdbcEnvironment) {
		return (name == null) ? null : Identifier.toIdentifier(format(name.getText()));
	}

	@Override
	public Identifier toPhysicalColumnName(Identifier name, JdbcEnvironment jdbcEnvironment) {
		return (name == null) ? null : Identifier.toIdentifier(format(name.getText()));
	}

	private String format(String name) {
		if (StringUtils.isEmpty(name)) {
			return name;
		}
		String joined = name.contains("_") ? name
				: StringUtils.join(StringUtils.splitByCharacterTypeCamelCase(name), '_');
		return joined.toUpperCase();
	}

}
