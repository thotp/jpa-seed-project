package com.thotp.seed.web.config;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.persistence.config.SessionCustomizer;
import org.eclipse.persistence.descriptors.ClassDescriptor;
import org.eclipse.persistence.internal.helper.Helper;
import org.eclipse.persistence.mappings.DatabaseMapping;
import org.eclipse.persistence.mappings.DirectToFieldMapping;
import org.eclipse.persistence.sessions.Session;

public class EclipseLinkSessionCustomizerImpl implements SessionCustomizer {

	@Override
	public void customize(Session session) throws Exception {
		// This code assumes single table per descriptor!
		for (ClassDescriptor desc : session.getDescriptors().values()) {
			String fullClassName = desc.getJavaClassName();
			String className = Helper.getShortClassName(fullClassName);
			String tableName = convertToUnderscore(className);
			desc.setTableName(tableName);
			
			for (DatabaseMapping mapping : desc.getMappings()) {
				if (mapping.isDirectToFieldMapping()) {
					DirectToFieldMapping directMapping = (DirectToFieldMapping) mapping;
					directMapping.getField().setTableName(tableName);
				}
			}
		}
	}

	private String convertToUnderscore(String name) {
		if (StringUtils.isEmpty(name)) {
			return name;
		}
		String joined = name.contains("_") ? name
				: StringUtils.join(StringUtils.splitByCharacterTypeCamelCase(name), '_');
		return joined.toUpperCase();
	}

}
